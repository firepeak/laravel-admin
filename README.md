# laravel-admin-v1

#### 介绍
- 使用laravel开发后台管理系统.版本V1
- laravel 使用版本5.8

### 软件架构
#### 简单的后台管理系统
- 后台管理员
- 角色管理
- 日志管理
- 菜单管理
- 配置管理

#### 前端使用
- 模块网址：http://ace.jeka.by/index.html
- `public/assets` 后台前端 样式
    - `demo-html.tar.gz` 前端页面，解压后将内容复制到`public/assets`目录下可浏览器打开访问静态页面.

## 安装教程
### 安装方式一
#### 安装前准备
```
chmod 777 -R ./storage         // 需要可写权限
chmod 777 -R ./bootstrap/cache        // 需要可写权限 

# 根目录下 修改.env配置文件，添加写入权限
cp .env.example .env
chmod 777 .env
```
- 已安装 `Composer`
    - 切换到项目根目录。将`composer.lock`删除，执行 `composer install` 安装第三方库。将会在根目录下创建`vendor`文件夹

#### 安装
- 1、配置好域名后,指向`public`.
- 2、将`public/install/install.lock`删除，访问域名安装.

- 软链接laravel文件存储 `storage/app/public` 地址到 `public/` 下创建 `public/storage`
```
php artisan storage:link
```

### 安装方式二
```
chmod 777 -R ./storage         // 需要可写权限
chmod 777 -R ./bootstrap/cache        // 需要可写权限 
```
- 配置laravel5.8运行环境
- 已安装 `Composer`
    - 切换到项目根目录。将`composer.lock`删除，执行 `composer install` 安装第三方库。将会在根目录下创建`vendor`文件夹
- 文件储存
    - 软链接laravel文件存储 `storage/app/public` 地址到 `public/` 下创建 `public/storage`
    ```
    php artisan storage:link
    ```

## 使用说明

### laravel框架命名
##### 清理缓存 shell 脚本
``` 
# 需要执行权限 
# chomd +x shell_cache.sh
shell_cache.sh
```

##### 清理配置文件缓存
```
php artisan config:cache
```

##### 清理视图缓存
```
php artisan view:clear
```

##### 清理运行缓存
```
php artisan cache:clear
```

##### 文件储存
- 软链接laravel文件存储 `storage/app/public` 地址到 `public/` 下创建 `public/storage`
    ```
    php artisan storage:link
    ```

##### 重新生成key
``` 
php artisan key:generate 
```

### 后台操作
##### 后台登录 生成登录链接
- `/admin/v1/common/login_token`
    - Username 登录后台的账户
    - Key `.env` 中 `ADMIN_KEY` 后台登录密钥
