@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>编辑</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" @if($myEdit == 1) action="my_edit" @else action="edit" @endif enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <input type="hidden" name="my_edit" value="{{ $myEdit }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right">名称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="username" value="{{ $info->username }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" readonly>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

{{--                    <div class="form-group warn-div">--}}
{{--                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 密码 </label>--}}
{{--                        <div class="col-sm-9 warn-div">--}}
{{--                            <input type="password" name="password" value="{{ $info->password }}" class="col-xs-10 col-sm-8" minlength="6" maxlength="18" placeholder="格式:长度6～18"--}}
{{--                                   autocomplete="off" required>--}}
{{--                            <div class="warn-span col-xs-10 col-sm-8"></div>--}}
{{--                        </div>--}}
{{--                    </div>--}}

                    <div class="form-group img_div">
                        <label class="col-sm-3 control-label no-padding-right"> 头像 </label>
                        <div class="col-sm-9">
                            <div>
                                <input type="text" name="head_portrait" value="{{ $info->head_portrait }}"  class="col-sm-6">
                                <input type="file" name="_img_head_portrait"  class="col-sm-3" onchange="preview_img(this);">
                            </div>
                            <div class="col-sm-9" style="margin: 0.5rem 0;">
                                @if(isset($info->head_portrait) && $info->head_portrait )
                                    <img src="{{ imageUrl($info->head_portrait) }}"  style="height: 2.5rem;width: 2.5rem;float: left;"/>
                                @endif
                                <div class="preview_img" style="margin-left:10rem;display: none;">
                                    <span>预览</span>
                                    <img src=""  style="height: 2.5rem;width: 2.5rem;"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 真实姓名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="realname" value="{{ $info->realname }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 联系电话 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="mobile" value="{{ $info->mobile }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 邮箱 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="email" value="{{ $info->email }}" class="col-xs-10 col-sm-8" maxlength="40" placeholder="格式:最大长度40"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>
                    @if($myEdit == '1')

                    @else
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 角色(可多选) </label>
                        <div class="col-sm-9 warn-div">
                            <input type="hidden" name="group_str" value="">
                            <div>
                                {!! From::checkbox($groupAll,$groupStr,' name="group" ','',70,'group') !!}
                            </div>
                            <div class="warn-span col-xs-10 col-sm-8 group"></div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">超级管理员 </label>
                        <div class="col-sm-9">
                            {!! From::radio($isSuperArray,isset($info->is_super)?$info->is_super:0,' name="is_super" ',70,'is_super') !!}
                        </div>
                    </div>
                    @endif

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 简介 </label>
                        <div class="col-sm-9">
                            <textarea name="introduction" class="col-xs-10 col-sm-8" rows="2" cols="20" style="height:150px;">{{ $info->introduction }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 后台登录token </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="login_token" value="{{ $info->login_token }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="后台登录地址"
                                   autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 后台登录链接 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="login_url" value="{{ $info->login_url }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="后台登录地址"
                                   autocomplete="off" readonly>
                        </div>
                    </div>


                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info form-submit" data-type="{{ $myEdit }}" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            $(".form-submit").on('click',function(){
                let myEdit = "{{ $myEdit }}";
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                });
                if(myEdit != '1') {
                    var checkId = [];//定义一个空数组
                    var group = "";
                    $("input[name=group]:checked").each(function (i) {//把所有被选中的复选框的值存入数组
                        checkId[i] = $(this).val();
                    });
                    if (checkId.length > 0) {
                        for (var i = 0; i < checkId.length; i++) {
                            group += checkId[i] + ',';
                        }
                        group = group.replace(/(,$)/, "");
                        $("input[name=group_str]").val(group);
                    }
                    $(".group").html('');
                    if (group == '') {
                        $(".group").html("<p>提示: 角色权限不能为空</p>");
                        _status = false;
                    }
                }
                if(_status){
                    $("#form").submit();
                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection

