@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>详情</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="edit" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right">名称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="username" value="{{ $info->username }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" readonly>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group img_div">
                        <label class="col-sm-3 control-label no-padding-right"> 头像 </label>
                        <div class="col-sm-9">
                            <div>
                                <input type="text" name="head_portrait" value="{{ $info->head_portrait }}"  class="col-sm-6">
{{--                                <input type="file" name="_img_head_portrait"  class="col-sm-3" onchange="preview_img(this);">--}}
                            </div>
                            <div class="col-sm-9" style="margin: 0.5rem 0;">
                                @if(isset($info->head_portrait) && $info->head_portrait )
                                    <img src="{{ imageUrl($info->head_portrait) }}"  style="height: 2.5rem;width: 2.5rem;float: left;"/>
                                @endif
                                <div class="preview_img" style="margin-left:10rem;display: none;">
                                    <span>预览</span>
                                    <img src=""  style="height: 2.5rem;width: 2.5rem;"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 真实姓名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="realname" value="{{ $info->realname }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 联系电话 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="mobile" value="{{ $info->mobile }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 邮箱 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="email" value="{{ $info->email }}" class="col-xs-10 col-sm-8" maxlength="40" placeholder="格式:最大长度40"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 角色 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="hidden" name="group_str" value="">
                            <div>
                                {!! From::checkbox($groupAll,$groupStr,' name="group" ','',70,'group') !!}
                            </div>
                            <div class="warn-span col-xs-10 col-sm-8 group"></div>
                        </div>

                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">超级管理员 </label>
                        <div class="col-sm-9">
                            {!! From::radio($isSuperArray,isset($info->is_super)?$info->is_super:0,' name="is_super" ',70,'is_super') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 简介 </label>
                        <div class="col-sm-9">
                            <textarea name="introduction" class="col-xs-10 col-sm-8" rows="2" cols="20" style="height:150px;">{{ $info->introduction }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 后台登录token </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="login_token" value="{{ $info->login_token }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="后台登录地址"
                                   autocomplete="off" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 后台登录链接 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="login_url" value="{{ $info->login_url }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="后台登录地址"
                                   autocomplete="off" readonly>
                        </div>
                    </div>


                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')
@endsection

