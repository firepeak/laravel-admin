@extends("admin.v1.common.mainPopUps")
@section("content")

    <div class="page-content">
        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" @if($myEdit == '1')action="my_pwd" @else action="pwd" @endif
                      enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right">名称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="username" value="{{ $info->username }}" class="col-xs-10 col-sm-8"
                                   minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" readonly>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 新密码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="password" name="password" value=""
                                   class="col-xs-10 col-sm-8"
                                   minlength="6" maxlength="18" placeholder="格式:长度6～18"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>


                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info form-submit" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->

    </div>
    <script>
        $(function(){
            $(".form-submit").on('click',function(){
                let myEdit = "{{ $myEdit }}";
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                let _names = [];
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                    $("input[name="+item.name+"]").parent("div").find(".warn-span").html("");
                    if(item.name == 'password'){
                        var html = "";
                        // var reg=/^[\w\d\u4e00-\u9fff,]{6,18}$/;
                        // var reg=/(?=^.{6,18}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[!@#$%^&*]).*$/;
                        var reg=/(?=^.{6,18}$)(?=.*\d)(?=.*[A-Z])(?=.*[a-z]).*$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 密码不能为空</p>";
                        }else if(!reg.test(item.value)){
                            // html = "<p>提示: 6-16位，,至少有一个数字，一个大写字母，一个小写字母和一个特殊字符，四个任意组合.</p>";
                            html = "<p>提示: 6-18位，至少有一个数字，一个大写字母，一个小写字母，三个任意组合.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                });
                if(_status){
                    // $("#form").submit();
                    let _url = '/admin/v1/admin_user/index/pwd';
                    if(myEdit == '1'){
                        _url = '/admin/v1/admin_user/index/my_pwd';
                    }
                    $.ajax({
                        url:_url,
                        type:"post",
                        dataType:"json",
                        data:data,
                        success:function (response) {
                            if(response.status && response.code=='000000'){
                                var _data = response.data;
                                layer.msg(response.message, {icon: 1,time:2000},function(){
                                    var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
                                    parent.layer.close(index);
                                });
                            }else{
                                layer.msg(response.message, {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            layer.close(layer.index);
                            if(jqXHR.status == 422 && textStatus == 'error'){
                                let responseError = jqXHR.responseJSON.errors;
                                $.each(responseError, function (index, item) {
                                    let html = "";
                                    $.each(item, function (key, val) {
                                        html += "<p>提示: "+val+"</p>";
                                    });
                                    $("input[name="+index+"]").parent("div").find(".warn-span").html(html);
                                })
                            }else if(jqXHR.status != 200){
                                layer.msg('请求错误', {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        }
                    })
                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection
