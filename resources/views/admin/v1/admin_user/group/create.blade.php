@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>详情</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create">
                    {{csrf_field()}}
                    <input type="hidden" name="menus" value="">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 名称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name" value="" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 描述 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="description" value="" class="col-xs-10 col-sm-8" maxlength="200" placeholder="格式:长度2～200"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"> 排序 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="list_order" value="888" autocomplete="off" maxlength="3" placeholder="格式:升序,1~999"
                                   class="col-xs-10 col-sm-8">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3  control-label no-padding-right"><code>*</code> 权限</label>
                        <div class="col-sm-6 warn-div">

                            <link rel="stylesheet" href="/assets/js/ztree/zTreeStyle.css" type="text/css">
                            <script type="text/javascript" src="/assets/js/ztree/jquery.ztree.core.min.js"></script>
                            <script type="text/javascript" src="/assets/js/ztree/jquery.ztree.excheck.min.js"></script>
                            <script type="text/javascript">
                                /**-
                                 * var zNodes = [
                                 {id: 11, pId: 1, name: "随意勾选 1-1", open: true},
                                 {id: 111, pId: 11, name: "随意勾选 1-1-1"},
                                 {id: 2, pId: 0, name: "随意勾选 2", checked: true, open: true},
                                 {id: 21, pId: 2, name: "随意勾选 2-1"},
                                 ];
                                 **/
                                var zNodes ={!! json_encode($menus) !!};
                                var setting = {
                                    check: {
                                        enable: true,
                                        chkStyle: "checkbox",
                                        chkboxType: {
                                            "Y": "ps",      //Y 属性定义 checkbox 被勾选后的情况； N 属性定义 checkbox 取消勾选后的情况；
                                            "N": "ps"       //“p” 表示操作会影响父级节点； “s” 表示操作会影响子级节点。
                                        }
                                    },
                                    data: {
                                        simpleData: {
                                            enable: true
                                        }
                                    }
                                };
                                $(document).ready(function () {
                                    $.fn.zTree.init($("#tree"), setting, zNodes);
                                    $("#dosubmit").click(function () {
                                        var menu = $.fn.zTree.getZTreeObj("tree").getCheckedNodes(true);
                                        var menus = '';
                                        for (var i = 0; i < menu.length; i++) {
                                            menus += menu[i].id + ',';
                                        }
                                        menus = menus.replace(/(,$)/, "");
                                        $("input[name='menus']").val(menus);
                                        // myform.submit();
                                    })
                                });
                            </script>
                            <style>
                                ul.ztree {
                                    margin-top: 10px;
                                    border: 1px solid #617775;
                                }
                            </style>
                            <ul id="tree" class="ztree" style="font-size: 20px;"></ul>
                            <div class="warn-span col-xs-10 col-sm-8" id="menu_warn"></div>
                        </div>
                    </div>

                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
{{--                            <button class="btn btn-info" type="submit" id="dosubmit">--}}
                            <button class="btn btn-info form-submit" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>

    <script>
        $(function(){
            $(".form-submit").on('click',function(){
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                    $("input[name="+item.name+"]").parent("div").find(".warn-span").html("");
                    if(item.name == 'name'){
                        var html = "";
                        var reg=/^[\w\d\u4e00-\u9fff,]{2,20}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 名称不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 格式错误</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                    if(item.name == 'description'){
                        var html = "";
                        var reg=/^[\w\d\u4e00-\u9fff,]{2,200}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 描述不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 格式错误</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                    if(item.name == 'list_order'){
                        var html = "";
                        var reg=/^\+?[1-9]{1}[0-9]{0,2}\d{0,0}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 排序不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 格式错误</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                    if(item.name == 'menus'){
                        var html = "";
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 权限菜单不能空</p>";
                        }
                        if(html != ""){
                            $("#menu_warn").html(html);
                            _status = false;
                        }
                    }

                });
                if(_status){
                    var index = layer.load(0, {shade: false});
                    $.ajax({
                        url:"/admin/v1/admin_user/group/create",
                        type:"post",
                        dataType:"json",
                        data:data,
                        success:function (response) {
                            layer.close(index);
                            if(response.status && response.code=='000000'){
                                var _data = response.data;
                                layer.msg(response.message, {icon: 1},function(){
                                    window.location.href = _data.url;
                                });
                            }else{
                                layer.msg(response.message, {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            layer.close(layer.index);
                            if(jqXHR.status == 422 && textStatus == 'error'){
                                let responseError = jqXHR.responseJSON.errors;
                                $.each(responseError, function (index, item) {
                                    let html = "";
                                    $.each(item, function (key, val) {
                                        html += "<p>提示: "+val+"</p>";
                                    });
                                    $("input[name="+index+"]").parent("div").find(".warn-span").html(html);
                                })
                            }else if(jqXHR.status != 200){
                                layer.msg('请求错误', {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        }
                    })
                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection

