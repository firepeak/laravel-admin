@extends("admin.v1.common.main")
@section("content")
<style>
    #simple-table tbody td{
        overflow:hidden;
        white-space:nowrap;
        overflow-x: scroll;
    }
</style>

    <div class="page-content">
        <div class="page-header">
            <h1>
                {{$menu_info->menu_name}}
            </h1>
        </div>

        <div class="operate panel panel-default">
            <div class="panel-body ">
                <form name="myform" method="GET" class="form-inline">
                    <div class="form-group select-input">
                        <div class="input-group">
                            <div class="input-group-addon">时间</div>
                            <input type="text" class="layui-input" id="start_time"  name="start_time"  value="{{request('start_time')}}">
                        </div>

                        <div class="input-group" style="margin-left: 0;">
                            <div class="input-group-addon"> 至</div>
                            <input type="text" class="layui-input" id="end_time"  name="end_time" value="{{request('end_time')}}">
                        </div>

                        <div class="input-group">
                            <div class="input-group-addon">菜单名称</div>
                            <input class="form-control" name="menu_name" type="text" value="{{request('menu_name')}}">
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon">管理员</div>
                            <input class="form-control" name="admin_name" type="text" value="{{request('admin_name')}}">
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon">ip地址</div>
                            <input class="form-control" name="ip" type="text" value="{{request('ip')}}">
                        </div>

                        <div class="input-group">
                            <div class="input-group-addon">类型</div>
                            {{From::select($typeArr,request('type'),'class="form-control" name="type"','--请选择--')}}
                        </div>

                        <div class="input-group" style="float: right;">
                            <input type="submit" value="搜索" class="btn btn-danger btn-sm">
                            <span class="btn btn-info btn-sm" onclick="window.location.href = '?'">重置</span>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <table id="simple-table" class="table  table-bordered table-hover" >
                            <thead>
                            <tr>
                                <th>日期</th>
                                <th>类型</th>
                                <th>菜单名称</th>
                                <th>请求方式</th>
                                <th>请求地址</th>
                                <th>ip地址</th>
                                <th>操作人</th>
                                <th>查看</th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach ($lists as $item)
                                <tr>
                                    <td>{{$item->created_at}}</td>
                                    <td>@if($item->type == 1)记录日志@elseif($item->type == 2)登录@elseif($item->type == 3)登出@endif</td>
                                    <td>{{$item->menu_name}}</td>
                                    <td>{{$item->method}}</td>
                                    <td style="min-width:400px;max-width: 500px" title="{{$item->m}}/{{$item->v}}/{{$item->address}}{{!empty($item->query_string)?'?'.$item->query_string:''}}">{{$item->m}}/{{$item->v}}/{{$item->address}}{{!empty($item->query_string)?'?'.$item->query_string:''}}</td>
                                    <td>{{$item->ip}}</td>
                                    <td>{{$item->admin_name}}</td>

                                    <td>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            @if(isPermission('admin_user/log/info',$_v))
                                                <a href="info?id={{$item->id}}">查看</a>
                                            @endif
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        <div id="page">
                            {{$lists->appends(request()->all())->links()}}
                            <div style="float: right;margin: 20px 0;">共<strong style="color: red;margin: 0 5px;">{{$lists->total()}}</strong>条</div>
                        </div>

                    </div><!-- /.span -->

                </div><!-- /.row -->
                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div>
    </div>



@endsection
