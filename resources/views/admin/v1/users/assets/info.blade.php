@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>详情</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="edit" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"> 资产变量名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name" value="{{ $info->name }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20;字母、下划线"
                                   autocomplete="off" disabled>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"> 资产变量别名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name_alias" value="{{ $info->name_alias }}" class="col-xs-10 col-sm-8" minlength="6" maxlength="18" placeholder="格式:长度2～20"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">是否初始化 </label>
                        <div class="col-sm-9">
                            {!! From::radio($isFirstArray,isset($info->is_first)?$info->is_first:0,' name="is_first" ',70,'is_first') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">用户资产</label>
                        <div class="col-sm-5">
                            <div class="widget-box">
                                <div class="widget-header widget-header-flat widget-header-small">
                                    <h5 class="widget-title">
                                        <i class="ace-icon fa fa-signal"></i>
                                        用户资产分布
                                    </h5>

                                </div>

                                <div class="widget-body">
                                    <div class="widget-main">
                                        <div id="piechart-placeholder" data-available="{{$info->user_assets->available}}" data-freeze="{{$info->user_assets->freeze}}"></div>

                                        <div class="hr hr8 hr-double"></div>

                                        <div class="clearfix">
                                            <div class="grid3">
															<span class="grey">
																<i class="iconfont blue">&#xe62d;</i>
																&nbsp; 总值
															</span>
                                                <h4 class="bigger pull-right">{{$info->user_assets->total}}</h4>
                                            </div>

                                            <div class="grid3">
															<span class="grey">
																<i class="iconfont purple">&#xe69e;</i>
																&nbsp; 可用值
															</span>
                                                <h4 class="bigger pull-right">{{$info->user_assets->available}}</h4>
                                            </div>

                                            <div class="grid3">
															<span class="grey">
																<i class="iconfont red">&#xe646;</i>
																&nbsp; 冻结值
															</span>
                                                <h4 class="bigger pull-right">{{$info->user_assets->freeze}}</h4>
                                            </div>
                                        </div>
                                    </div><!-- /.widget-main -->
                                </div><!-- /.widget-body -->
                            </div><!-- /.widget-box -->
                        </div>
                    </div>
                </form>


            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>


    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="/assets/js/jquery-2.1.4.min.js"></script>

    <!-- <![endif]-->

    <!--[if IE]>
    <script src="/assets/js/jquery-1.11.3.min.js"></script>
    <![endif]-->
    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
    <script src="/assets/js/bootstrap.min.js"></script>

    <!-- page specific plugin scripts -->

    <!--[if lte IE 8]>
    <script src="/assets/js/excanvas.min.js"></script>
    <![endif]-->
    <script src="/assets/js/jquery-ui.custom.min.js"></script>
    <script src="/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="/assets/js/jquery.easypiechart.min.js"></script>
    <script src="/assets/js/jquery.sparkline.index.min.js"></script>
    <script src="/assets/js/jquery.flot.min.js"></script>
    <script src="/assets/js/jquery.flot.pie.min.js"></script>
    <script src="/assets/js/jquery.flot.resize.min.js"></script>

    <!-- ace scripts -->
    <script src="/assets/js/ace-elements.min.js"></script>
    <script src="/assets/js/ace.min.js"></script>

    <!-- inline scripts related to this page -->

    <script type="text/javascript">
        jQuery(function($) {
            var _form = $('#form').serializeArray();
            $.each(_form, function (index, item) {
                $("input[name="+item.name+"]").attr('disabled','disabled');
            });


            $.resize.throttleWindow = false;
            var placeholder = $('#piechart-placeholder').css({'width':'90%' , 'min-height':'150px'});
            var available = $('#piechart-placeholder').data('available');
            var freeze = $('#piechart-placeholder').data('freeze');
            var data = [
                { label: "可用值",  data: available, color: "#68BC31"},
                { label: "冻结",  data: freeze, color: "#DA5430"},
            ]
            function drawPieChart(placeholder, data, position) {
                $.plot(placeholder, data, {
                    series: {
                        pie: {
                            show: true,
                            tilt:0.8,
                            highlight: {
                                opacity: 0.25
                            },
                            stroke: {
                                color: '#fff',
                                width: 2
                            },
                            startAngle: 2
                        }
                    },
                    legend: {
                        show: true,
                        position: position || "ne",
                        labelBoxBorderColor: null,
                        margin:[-30,15]
                    }
                    ,
                    grid: {
                        hoverable: true,
                        clickable: true
                    }
                })
            }
            drawPieChart(placeholder, data);

            /**
             we saved the drawing function and the data to redraw with different position later when switching to RTL mode dynamically
             so that's not needed actually.
             */
            placeholder.data('chart', data);
            placeholder.data('draw', drawPieChart);


            //pie chart tooltip example
            var $tooltip = $("<div class='tooltip top in'><div class='tooltip-inner'></div></div>").hide().appendTo('body');
            var previousPoint = null;

            placeholder.on('plothover', function (event, pos, item) {
                if(item) {
                    if (previousPoint != item.seriesIndex) {
                        previousPoint = item.seriesIndex;
                        var tip = item.series['label'] + " : " + item.series['percent'].toFixed(6)+'%';
                        $tooltip.show().children(0).text(tip);
                    }
                    $tooltip.css({top:pos.pageY + 10, left:pos.pageX + 10});
                } else {
                    $tooltip.hide();
                    previousPoint = null;
                }

            });
        });
    </script>
@endsection

