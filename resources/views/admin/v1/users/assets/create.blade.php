@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>创建</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 资产变量名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name" value="" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20;字母、下划线"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 资产变量别名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name_alias" value="" class="col-xs-10 col-sm-8" minlength="6" maxlength="18" placeholder="格式:长度2～20"
                                   autocomplete="off" required>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">是否初始化 </label>
                        <div class="col-sm-9">
                            {!! From::radio($isFirstArray,isset($info->is_first)?$info->is_first:0,' name="is_first" ',70,'is_first') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>


                    <div class="clearfix form-actions">
                        <div class="col-md-offset-3 col-md-9">
                            <button class="btn btn-info form-submit" type="button" id="dosubmit">
                                <i class="ace-icon fa fa-check bigger-110"></i>
                                提交
                            </button>
                            <button class="btn reset" type="reset">
                                <i class="ace-icon fa fa-undo bigger-110"></i>
                                Reset
                            </button>
                        </div>
                    </div>
                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            $(".form-submit").on('click',function(){
                let data = {};
                let value = $('#form').serializeArray();
                let _status = true;
                let _names = [];
                $.each(value, function (index, item) {
                    data[item.name] = $.trim(item.value);
                    $("input[name="+item.name+"]").parent("div").find(".warn-span").html("");
                    if(item.name == 'name'){
                        var html = "";
                        var reg=/^[A-Za-z]{1}[A-Za-z_-]{2,20}$/;
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 名称不能为空</p>";
                        }else if(!reg.test(item.value)){
                            html = "<p>提示: 格式错误;必须是以字母开头，只能包含字母下划线和减号，2到20位.</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                    if(item.name == 'name_alias'){
                        var html = "";
                        if((item.value == null || item.value == '')){
                            html = "<p>提示: 别名不能为空</p>";
                        }
                        if(html != ""){
                            $("input[name="+item.name+"]").parent("div").find(".warn-span").html(html);
                            _status = false;
                        }
                    }
                });

                if(_status){
                    var index = layer.load(0, {shade: false});
                    $.ajax({
                        url:"/admin/v1/users/assets/create",
                        type:"post",
                        dataType:"json",
                        data:data,
                        success:function (response) {
                            layer.close(index);
                            if(response.status && response.code=='000000'){
                                var _data = response.data;
                                layer.msg(response.message, {icon: 1},function(){
                                    window.location.href = _data.url;
                                });

                            }else{
                                layer.alert(response.message,{
                                    icon: 2,
                                    title:'提示'
                                });
                            }
                        },
                        error:function(jqXHR, textStatus, errorThrown){
                            layer.close(layer.index);
                            if(jqXHR.status == 422 && textStatus == 'error'){
                                let responseError = jqXHR.responseJSON.errors;
                                $.each(responseError, function (index, item) {
                                    let html = "";
                                    $.each(item, function (key, val) {
                                        html += "<p>提示: "+val+"</p>";
                                    });
                                    $("input[name="+index+"]").parent("div").find(".warn-span").html(html);
                                })
                            }else if(jqXHR.status != 200){
                                layer.msg('请求错误', {
                                    icon: 2,
                                    // time: 20000, //20s后自动关闭
                                    btn: ['知道了']
                                });
                            }
                        }
                    });
                }
            });
            $(".reset").on('click',function(){
                $(".warn-span").html("");
            });
        });
    </script>
@endsection

