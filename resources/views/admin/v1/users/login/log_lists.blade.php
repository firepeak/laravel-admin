@extends("admin.v1.common.main")
@section("content")

<div class="page-content">
    <div class="page-header">
        <h1>
            {{$menu_info->menu_name}}
        </h1>
    </div>

    <div class="operate panel panel-default">
        <div class="panel-body ">
            <form name="myform" method="GET" class="form-inline">
                <div class="form-group select-input">
                    <div class="input-group">
                        <div class="input-group-addon">时间</div>
                        <input type="text" class="layui-input" id="start_time"  name="start_time"  value="{{request('start_time',$start_time)}}" autocomplete="off">
                    </div>

                    <div class="input-group" style="margin-left: 0;">
                        <div class="input-group-addon"> 至</div>
                        <input type="text" class="layui-input" id="end_time"  name="end_time" value="{{request('end_time')}}" autocomplete="off">
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon">类型</div>
                        {{From::select($typeArr,request('type'),'class="form-control" name="type"','--请选择--')}}
                    </div>

                    <div class="input-group">
                        <div class="input-group-addon">用户ID</div>
                        <input class="form-control" name="user_id" type="text" value="{{request('user_id')}}" placeholder="" autocomplete="off">
                    </div>

                    <div class="input-group" style="float: right;">
                        <input type="submit" value="搜索" class="btn btn-danger btn-sm">
                        <span class="btn btn-info btn-sm" onclick="window.location.href = '?'">重置</span>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="row">
	<div class="col-xs-12">
	    <!-- PAGE CONTENT BEGINS -->
	    <div class="row">
		<div class="col-xs-12">
		    <table id="simple-table" class="table  table-bordered table-hover" >
			<thead style="white-space: nowrap;overflow: scroll;">
			    <tr>
				<th>用户ID</th>
				<th>类型</th>
				<th>IP</th>
				<th>关联平台</th>
				<th>备注</th>
				<th>创建时间</th>
			    </tr>
			</thead>

			<tbody>
            @if(isset($lists))
			    @foreach ($lists as $info)
			    <tr>
				<td>{{$info->user_id}}</td>
				<td>{{isset($typeArr[$info->type])?$typeArr[$info->type]:''}}</td>
				<td>{{$info->ip}}</td>
				<td>{{$info->account}}</td>
				<td style="white-space: nowrap;overflow: scroll;max-width: 15rem;">{{$info->remark}}</td>
				<td style="max-width: 200px;min-width: 150px;">{{$info->created_at}}</td>
			    </tr>
			    @endforeach
            @endif
			</tbody>
		    </table>
            @if(isset($lists))
                <div id="page">
                    {{$lists->appends(request()->all())->links()}}
                    <div style="float: right;margin: 20px 0;">共<strong style="color: red;margin: 0 5px;">{{$lists->total()}}</strong>条</div>
                </div>
            @endif
		</div><!-- /.span -->

	    </div><!-- /.row -->
	    <!-- PAGE CONTENT ENDS -->
	</div><!-- /.col -->
    </div>
 </div>
@endsection
