@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>创建</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"><code>*</code> 账户 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="account" value="{{ $info->account }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" disabled="disabled">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 昵称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="nickname" value="{{ $info->nickname }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 邮箱 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="email" value="{{ $info->email }}" class="col-xs-10 col-sm-8" maxlength="30" placeholder="格式:最大长度30"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 手机号码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="mobile_phone" value="{{ $info->mobile_phone }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> QQ </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="qq" value="{{ $info->qq }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 微信 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="wechat" value="{{ $info->wechat }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 真实姓名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="real_name" value="{{ $info->real_name }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 身份证ID </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="id_card" value="{{ $info->id_card }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">性别 </label>
                        <div class="col-sm-9">
                            {!! From::radio($sexArray,isset($info->sex)?$info->sex:0,' name="sex" ',70,'sex') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">认证 </label>
                        <div class="col-sm-9">
                            {!! From::radio($certificationArray,isset($info->is_certification)?$info->is_certification:0,' name="is_certification" ',70,'is_certification') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 用户邀请码 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="invite_code" value="{{ $info->invite_code }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder=""
                                   autocomplete="off" disabled>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 推荐用户ID </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="recommend_id" value="{{ $info->recommend_id }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder=""
                                   autocomplete="off" disabled>
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>
                    <!-- 资产信息 -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 资产信息 </label>
                        <div class="col-sm-9 warn-div">
                            <table id="simple-table" class="table table-bordered table-hover col-xs-10 col-sm-8" style="width:auto;">
                                <thead>
                                <tr>
                                    <th>资产名称</th>
                                    <th>总值</th>
                                    <th>可用值</th>
                                    <th>冻结值</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if($info->user_assets)
                                    @foreach($info->user_assets as $key=>$val)
                                        <tr>
                                            <td>{{ $val->name_alias }}</td>
                                            <td>{{ $val->total }}</td>
                                            <td>{{ $val->available }}</td>
                                            <td>{{ $val->freeze }}</td>
                                        </tr>
                                    @endforeach
                                    @else
                                    <tr>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                        <td>-</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            var _form = $('#form').serializeArray();
            $.each(_form, function (index, item) {
                $("input[name="+item.name+"]").attr('disabled','disabled');
            });
        });
    </script>
@endsection

