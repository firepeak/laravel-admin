@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">


        <div class="page-header">
            <h1>
                <span>编辑</span>
                <div style="width: 50%; font-size: 0.7rem;display: inline-block;">
                    <a href="/admin"><span>主页</span></a>
                    @foreach($menu_parent as $items)
                        <a href="{{$items['path_url']}}"><span>/{{$items['menu_name']}}</span></a>
                    @endforeach
                </div>
                <button class="btn btn-sm btn-primary pull-right" onclick="javascript:window.location.href = 'lists'">
                    返回列表
                </button>
            </h1>

        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">

            @include("admin.v1.common.error")
            <!-- PAGE CONTENT BEGINS -->
                <form id="form" name="myform" class="form-horizontal" role="form" method="POST" action="create" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <input type="hidden" name="id" value="{{ $info->id }}">
                    <div class="form-group warn-div">
                        <label class="col-sm-3 control-label no-padding-right"> 账户 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="account" value="{{ $info->account }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off" disabled="disabled">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 平台名称 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="name" value="{{ $info->name }}" class="col-xs-10 col-sm-8" minlength="2" maxlength="20" placeholder="格式:长度2～20"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 平台IP </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="ip" value="{{ $info->ip }}" class="col-xs-10 col-sm-8" maxlength="20" placeholder="格式:最大长度30"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 访问域名 </label>
                        <div class="col-sm-9 warn-div">
                            <input type="text" name="domain_name" value="{{ $info->domain_name }}" class="col-xs-10 col-sm-8" maxlength="40" placeholder="格式:最大长度50"
                                   autocomplete="off">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right">状态 </label>
                        <div class="col-sm-9">
                            {!! From::radio($statusArray,isset($info->status)?$info->status:1,' name="status" ',70,'status') !!}
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 简介 </label>
                        <div class="col-sm-9">
                            <textarea name="introduction" class="col-xs-10 col-sm-8" rows="2" cols="20" maxlength="200" style="height:150px;">{{ $info->introduction }}</textarea>
                        </div>
                    </div>

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

    <script>
        $(function(){
            var _form = $('#form').serializeArray();
            $.each(_form, function (index, item) {
                $("input[name="+item.name+"]").attr('disabled','disabled');
            });
        });
    </script>
@endsection

