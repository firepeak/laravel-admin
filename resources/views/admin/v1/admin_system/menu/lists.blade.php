@extends("admin.v1.common.main")
@section("content")

    <div class="page-content">
        <div class="page-header">
            <h1>
                {{$menu_info->menu_name}}
                @if(isPermission('system/menu/create',$_v))
                    <span class="btn btn-sm btn-primary pull-right"
                          onclick="javascript:window.location.href = 'create'">创建</span>
                @endif
            </h1>
        </div>
        <div class="operate panel panel-default">
            <div class="panel-body ">
                <form name="myform-select" method="GET" class="form-inline">
                    <div class="form-group select-input">
                        <div class="input-group">
                            <div class="input-group-addon">主菜单名称</div>
                            <input class="form-control" name="menu_name" type="text" value="{{request('menu_name')}}"   placeholder="菜单名称">
                        </div>
{{--                        <div class="input-group">--}}
{{--                            <div class="input-group-addon">菜单类型</div>--}}
{{--                            {{From::select($menuType,request('is_type'),'class="form-control" name="is_type"','--请选择--')}}--}}
{{--                        </div>--}}
                        <div class="input-group">
                            <div class="input-group-addon">菜单状态</div>
                            {{From::select($menuDisplay,request('is_display'),'class="form-control" name="is_display"','--请选择--')}}
                        </div>
                        <div class="input-group">
                            <div class="input-group-addon">记录日志状态</div>

                            {{From::select($menuWriteLog,request('write_log'),'class="form-control" name="write_log"','--请选择--')}}
                        </div>
                        <div class="input-group">
                            <input type="submit" value="搜索" class="btn btn-danger btn-sm">
                            <span class="btn btn-info btn-sm" onclick="window.location.href = '?'">重置</span>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <div class="row">
                    <div class="col-xs-12">
                        <table id="simple-table" class="table  table-bordered table-hover">
                            <thead>
                            <tr>
                                <th class="detail-col">Id</th>
                                <th class="detail-col" width="10">排序</th>
                                <th>名称</th>
                                <th>模版</th>
                                <th>地址</th>
                                <th class="hidden-480">data</th>
                                <th class="hidden-480">类型</th>
                                <th class="hidden-480">状态</th>
                                <th class="hidden-480">日志记录</th>
                                <th>管理员操作</th>
                            </tr>
                            </thead>

                            <tbody>

                            <form action="" method="post" name="myform" id="listsForm">
                            {{csrf_field()}}
                            @foreach($lists as $info)
                            <tr id="lists_tr_{{$info['id']}}">
                                <td>{{$info['id']}}</td>
                                <td><input type="text" name="list_order[{{$info['id']}}]" value="{{$info['list_order']}}" width="10"></td>
                                <td><span class="fa {{$info['icon']}}"></span> {{$info['menu_name']}}</td>
                                <td>{{$info['m']}}/{{$info['v']}}</td>
                                <td>{{$info['address']}}</td>
                                <td class="hidden-480">
                                    <span class="label label-sm label-warning">{{$info['data']}}</span>
                                </td>
                                <td>
                                    @if(isset($menuType[$info['is_type']]))
                                        {{ $menuType[$info['is_type']] }}
                                    @endif
                                </td>
                                <td>
                                    @if(isset($menuDisplay[$info['is_display']]))
                                        {{ $menuDisplay[$info['is_display']] }}
                                    @endif
                                </td>
                                <td>
                                    @if(isset($menuWriteLog[$info['write_log']]))
                                        {{ $menuWriteLog[$info['write_log']] }}
                                    @endif
                                </td>
                                <td>
                                    <div class="hidden-sm hidden-xs btn-group">
                                        @if(isPermission('system/menu/info',$_v))
                                            <a href="info?id={{$info['id']}}" title="详情">
                                            <span class="btn btn-xs btn-warning">
                                        <i class="ace-icon fa fa-flag bigger-120"></i>
                                                </span>
                                            </a>
                                        @endif
                                        @if(isPermission('system/menu/create',$_v))
                                            <a href="create?parent_id={{$info['id']}}" title="添加">
                                            <span class="btn btn-xs btn-success">
                                        <i class="ace-icon fa fa-plus-square-o bigger-120"></i>
                                                </span>
                                            </a>
                                        @endif
                                        @if(isPermission('system/menu/edit',$_v))
                                            <a href="edit?id={{$info['id']}}" title="编辑">
                                            <span class="btn btn-xs btn-info">
                                        <i class="ace-icon fa fa-pencil bigger-120"></i>
                                            </span>
                                            </a>
                                        @endif
                                        @if(isPermission('system/menu/del',$_v))
                                           <a class="lists_tr_del" data-id="{{$info['id']}}" data-token="{{csrf_token()}}" title="删除">
                                            <span class="btn btn-xs btn-danger">
                                        <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                            </a>
                                        @endif
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                            </form>
                            </tbody>
                        </table>
{{--                        <span class="btn btn-info"  onclick="myform.action='edit';myform.submit();">排序</span>--}}
                        <span class="btn btn-info lists_sequence">排序</span>
                    </div><!-- /.span -->
                </div><!-- /.row -->


                <!-- PAGE CONTENT ENDS -->
            </div><!-- /.col -->
        </div>
    </div>


<script>
    $(function(){
        $(".lists_tr_del").on('click',function(){
            var _id = $(this).data('id');
            var _token = $(this).data('token');
            var index = layer.load(0, {shade: false});
            $.ajax({
                url:"/admin/v1/system/menu/del",
                type:"delete",
                dataType:"json",
                data:{id:_id,_token:_token},
                success:function (response) {
                    layer.close(index);
                    if(response.status && response.code=='000000'){
                        var _data = response.data;
                        var _ids = _data.ids;
                        layer.msg(response.message, {icon: 1});
                        $.each(_ids, function (index, item) {
                            $("#lists_tr_"+item).remove();
                        });
                    }else{
                        layer.msg(response.message, {
                            icon: 2,
                            // time: 20000, //20s后自动关闭
                            btn: ['知道了']
                        });
                    }
                },
            })
        });
        $(".lists_sequence").on('click',function(){
            let listsForm = $('#listsForm').serializeArray();
            let data = {};
            $.each(listsForm,function(index,item){
                data[item.name] = $.trim(item.value);
            });
            data['edit_type'] = 'list_order';
            var index = layer.load(0, {shade: false});
            $.ajax({
                url:"/admin/v1/system/menu/edit",
                type:"post",
                dataType:"json",
                data:data,
                success:function (response) {
                    layer.close(index);
                    if(response.status && response.code=='000000'){
                        layer.msg(response.message, {icon: 1});
                    }else{
                        layer.msg(response.message, {
                            icon: 2,
                            // time: 20000, //20s后自动关闭
                            btn: ['知道了']
                        });
                    }
                }
            });
        });
    });
</script>
@endsection
