@extends("admin.v1.common.main")
@section("content")
    <div class="page-content">

        <div class="page-header">
            <h1>
                {{ $menu_info->menu_name}}
            </h1>
        </div><!-- /.page-header -->

        <div class="row">
            <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
                <form id="form" class="form-horizontal" role="form" method="POST" action="save" enctype="multipart/form-data">
                    {{csrf_field()}}

                    <div class="form-group">
                        <label class="col-sm-3 control-label no-padding-right"> 后台名称 </label>
                        <div class="col-sm-9">
                            <input type="hidden" name="config[backstage_name][name]" value="backstage_name">
                            <input type="hidden" name="config[backstage_name][alias]" value="后台名称">
                            <input type="hidden" name="config[backstage_name][type_name]" value="backstage_set">
                            <input type="text" name="config[backstage_name][value]" value="@if(isset($config->backstage_set['backstage_name']->value)){{ $config->backstage_set['backstage_name']->value }}@endif"  class="col-xs-10 col-sm-8">
                            <div class="warn-span col-xs-10 col-sm-8"></div>
                        </div>
                    </div>
                    <div class="form-group img_div">
                        <label class="col-sm-3 control-label no-padding-right"> 标题 icon </label>
                        <div class="col-sm-9">
                            <div>
                                <input type="hidden" name="config[backstage_icon_url][name]" value="backstage_icon_url">
                                <input type="hidden" name="config[backstage_icon_url][alias]" value="标题icon">
                                <input type="hidden" name="config[backstage_icon_url][type_name]" value="backstage_set">
                                <input type="text" name="config[backstage_icon_url][value]" value="@if(isset($config->backstage_set['backstage_icon_url']->value)){{ $config->backstage_set['backstage_icon_url']->value }}@endif"  class="col-sm-6">
                                <input type="file" name="_img_backstage_icon_url"  class="col-sm-3" onchange="preview_img(this);">
                            </div>
                            <div class="col-sm-9" style="margin: 0.5rem 0;">
                                @if(isset($config->backstage_set['backstage_icon_url']->value) && $config->backstage_set['backstage_icon_url']->value )
                                    <img src="{{ imageUrl($config->backstage_set['backstage_icon_url']->value) }}"  style="height: 2.5rem;width: 2.5rem;float: left;"/>
                                @endif
                                <div class="preview_img" style="margin-left:10rem;display: none;">
                                    <span>预览</span>
                                    <img src=""  style="height: 2.5rem;width: 2.5rem;"/>
                                </div>
                            </div>
                        </div>
                    </div>

                    @if(isPermission('system/config/save',$_v))
                        <div class="clearfix form-actions">
                            <div class="col-md-offset-3 col-md-9">
                                <button class="btn btn-info" type="submit">
                                    <i class="ace-icon fa fa-check bigger-110"></i>
                                    提交
                                </button>

                                &nbsp; &nbsp; &nbsp;
                                <button class="btn" type="reset">
                                    <i class="ace-icon fa fa-undo bigger-110"></i>
                                    Reset
                                </button>
                            </div>
                        </div>
                    @endif

                </form>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div>
    @include('admin.v1.common.img')

@endsection


