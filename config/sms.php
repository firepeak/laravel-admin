<?php
return [
    'aliyun' => [
        'sign_name'   => env('SMS_ALIYUN_SIGN_NAME', 'null'),
        'key_id' => env('SMS_ALIYUN_KEY_ID', 'null'),
        'key_secret' => env('SMS_ALIYUN_KEY_SECRET', 'null'),
        'template_code_verify_code' => env('SMS_ALIYUN_VERIFY_CODE', 'null'),
    ],
];
