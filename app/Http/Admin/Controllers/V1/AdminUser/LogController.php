<?php


namespace App\Http\Admin\Controllers\V1\AdminUser;


use App\Http\Admin\Controllers\V1\BasisController;
use App\Models\AdminLog;

class LogController extends BasisController
{
    public function lists(){
        $condition = [
            'keyword' => request('keyword'),
            'type' => request('type'),
            'menu_name' => request('menu_name'),
            'username' => request('username'),
            'ip' => request('ip'),
            'start_time' => request('start_time'),
            'end_time' => request('end_time'),
        ];
        $page = request('page',1);
        $pageSize = request('page_size',20);
        $modelLog = new AdminLog();
        $lists = $modelLog->paginateLists($condition,[],[],$page,$pageSize);
        $typeArr = $modelLog->typeArr();
        return $this->view(compact(array('lists','typeArr')),'admin_user/log/lists');
    }

    public function info(){
        $modelLog = new AdminLog();
        $info = $modelLog->getInfo(request('id'));

        //上次信息
        $where=[];
        $where[]=['menu_id',$info->menu_id];
        $where[]=['primary_id',$info->primary_id];
        $where[] = ['id','<',$info->id];
        $last_id = $modelLog->where($where)->orderBy('id','desc')->value('id');

        if($last_id){
            $last_info = $modelLog->getInfo($last_id);
        }else{
            $last_info=[];
        }
        $info->data = json_decode($info->data,true);
        if(!empty($last_info)){
            if($info->data && $last_info->data ){
                $last_info->data = @json_decode($last_info->data,true);
                $last_info->data = $this->diffArr($info->data,$last_info->data);
            }
        }
        return $this->view(compact(array('info','last_info')),'admin_user/log/info');
    }

    //对比
    private function diffArr($info,$last_info,$key=''){
        static $arr;
        foreach($info as $k=>$v){
            if(!is_array($v)){
                if($v!=$last_info[$k]){
                    if($key){
                        $arr[$key][$k] ='<font color="red">'.$last_info[$k].'</font>';
                    }else{
                        $arr[$k] ='<font color="red">'.$last_info[$k].'</font>';
                    }
                }else{
                    $arr[$k]=$last_info[$k];
                }
            }else{
                return $this->diffArr($v,$last_info[$k],$k);
            }
        }
        return $arr;
    }
}
