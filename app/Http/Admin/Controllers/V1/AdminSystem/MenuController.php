<?php


namespace App\Http\Admin\Controllers\V1\AdminSystem;


use App\Http\Admin\Controllers\V1\BasisController;
use App\Libs\Common\Cache\PermissionMenu;
use App\Models\AdminMenu;

class MenuController extends BasisController
{
    private $modeMenu;

    public function __construct()
    {
        parent::__construct();
        $this->modeMenu = new AdminMenu();
    }

    public function lists(){
        $where=[];
        if(is_numeric(request('is_type'))){
            $where[]=['is_type',request('is_type')];
        }
        if(is_numeric(request('is_display'))){
            $where[]=['is_display',request('is_display')];
        }
        if(is_numeric(request('write_log'))){
            $where[]=['write_log',request('write_log')];
        }
        if(request('menu_name')){
            $where[]=['menu_name','like','%'.request('menu_name').'%'];
        }
        $lists = $this->modeMenu->getMenuList($where);
        foreach ($lists as $k => $v) {
            $lists[$k]['menu_name'] = $v['level'] == 0 ?  $v['menu_name']  : '├─' . $v['menu_name'];
            $lists[$k]['menu_name'] = str_repeat("│ ", $v['level']) . $lists[$k]['menu_name'];
        }

        $menuType = $this->modeMenu->isTypeArray();
        $menuDisplay = $this->modeMenu->isDisplayArray();
        $menuWriteLog = $this->modeMenu->writeLogArray();
        return $this->view(compact(array('lists','menuType','menuDisplay','menuWriteLog')),'admin_system/menu/lists');
    }

    public function info(){
        $info = $this->modeMenu->find(request('id'));
        $where[] = ['is_display','=',1];
        $menus = $this->modeMenu->selectMenu($where);

        $menuType = $this->modeMenu->isTypeArray();
        $menuDisplay = $this->modeMenu->isDisplayArray();
        $menuWriteLog = $this->modeMenu->writeLogArray();
        return $this->view(compact(array('info','menus','menuType','menuDisplay','menuWriteLog')),'admin_system/menu/info');
    }

    public function create(){
        if(request()->isMethod('post')) {
            $storeInfo = $this->storeInfo();
            if ($storeInfo['status']) {
                return $this->responseMsg('000000',true,'创建成功',array('url'=>'/admin/v1/system/menu/lists'));
            }
            return $this->responseMsg('200000',false,"创建失败;{$storeInfo['message']}",array('url'=>'/admin/v1/system/menu/lists'));
        }
        $where[] = ['is_display','=',1];
        $menus = $this->modeMenu->selectMenu($where);

        $menuType = $this->modeMenu->isTypeArray();
        $menuDisplay = $this->modeMenu->isDisplayArray();
        $menuWriteLog = $this->modeMenu->writeLogArray();
        return $this->view(compact(array('info','menus','menuType','menuDisplay','menuWriteLog')),'admin_system/menu/create');
    }

    public function edit(){
        if(request()->isMethod('post')) {
            $editType = request('edit_type');
            if($editType == 'list_order'){
                return $this->setListOrder();
            }

            $storeInfo = $this->storeInfo();
            if ($storeInfo['status']) {
                return $this->responseMsg('000000',true,'编辑成功',array('url'=>'/admin/v1/system/menu/lists'));
            }
            return $this->responseMsg('200000',false,"编辑失败;{$storeInfo['message']}",array('url'=>'/admin/v1/system/menu/lists'));
        }

        $info = $this->modeMenu->find(request('id'));
        $where[] = ['is_display','=',1];
        $menus = $this->modeMenu->selectMenu($where);
        $menuType = $this->modeMenu->isTypeArray();
        $menuDisplay = $this->modeMenu->isDisplayArray();
        $menuWriteLog = $this->modeMenu->writeLogArray();
        return $this->view(compact(array('info','menus','menuType','menuDisplay','menuWriteLog')),'admin_system/menu/edit');
    }

    private function storeInfo(){
        try {
            $this->validate(request(), $this->modeMenu->rules, $this->modeMenu->messages);
            $params = request($this->modeMenu->fillable); //可以添加或修改的参数
            if ($params['parent_id'] === null) {
                $params['parent_id'] = 0;
            }
            if (request('id')) {
                $params['updated_user'] = $this->adminUser->id;
                $res = $this->modeMenu->where('id', request('id'))->update($params);
            } else {
                $params['creator_user'] = $this->adminUser->id;
                $res = $this->modeMenu->create($params);
            }
            if (!empty($res)) {
                PermissionMenu::allMenu(1);        // 重新缓存菜单
                cleanUpViewCache();
                cleanUpCache();
                return responseArray('000000',true,'Success');
            }
        }catch (\Exception $e){
            return responseArray('200000',false,"Error:: {$e->getMessage()}");
        }
        return responseArray('200000',false,"Error");
    }

    //删除
    public function del()
    {
        $id = intval(request('id'));
        $menuChild = PermissionMenu::menuChild($id);
        $ids = $menuChild['ids'];
        $ids[] = $id;
        $res = $this->modeMenu->whereIn('id',$ids)->delete();
        if($res){
            PermissionMenu::allMenu(1);        // 重新缓存菜单
            cleanUpViewCache();
            cleanUpCache();
            return $this->responseMsg('000000',true,'删除成功',array('url'=>'/admin/v1/system/menu/lists','ids'=>$ids));
        }
        return $this->responseMsg('200000',false,'删除失败',array('url'=>'/admin/v1/system/menu/lists'));
    }

    //排序
    public function setListOrder(){
        $data = request('list_order');
        foreach($data as $k=>$v){
            $this->modeMenu->where('id',$k)->update(['list_order'=>$v]);
        }
        PermissionMenu::allMenu(1);        // 重新缓存菜单
        cleanUpViewCache();
        cleanUpCache();
        return $this->responseMsg('000000',true,'编辑成功',array('url'=>'/admin/v1/system/menu/lists'));
    }


}
