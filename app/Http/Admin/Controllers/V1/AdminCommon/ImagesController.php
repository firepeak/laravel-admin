<?php


namespace App\Http\Admin\Controllers\V1\AdminCommon;


use App\Http\Admin\Controllers\V1\BasisController;
use Illuminate\Support\Facades\DB;

class ImagesController extends BasisController
{
    public function upload(){
        $hasFile = request('_img');
        if(\request()->hasFile("{$hasFile}")){
            $imgResult = uploadImage("{$hasFile}");
            if($imgResult['status']){
                    return $this->responseMsg('000000',true,'Success',array('url'=>'/admin/v1/system/config/index'));
            }else{
                return $this->responseMsg($imgResult['code'],$imgResult['status'],$imgResult['message'],array('url'=>'/admin/v1/system/config/index'));
            }
        }
    }

}
