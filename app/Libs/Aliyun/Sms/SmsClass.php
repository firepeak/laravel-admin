<?php


namespace App\Libs\Aliyun\Sms;


class SmsClass
{
    private $signName = null;
    private $keyId = null;
    private $keySecret = null;
    private $smsConfig = [];

    public function __construct()
    {
        $sms_config = config('sms.aliyun');
        $this->signName = $sms_config['sign_name'];
        $this->keyId = $sms_config['key_id'];
        $this->keySecret = $sms_config['key_secret'];
        $this->smsConfig = $sms_config;
    }


    /**
     * Todo:: Verification code
     * @param $data
     * @return mixed
     */
    public function sendVerifyCode($data)
    {
        $params["SignName"] = $this->signName;
        $params["PhoneNumbers"] = $data['mobile'];
        $params["TemplateCode"] = $this->smsConfig['template_code_verify_code'];
        $params['TemplateParam'] = json_encode(array(
            'code' => $data['code']
        ), JSON_UNESCAPED_UNICODE);

        return $this->request($params);
    }

    /**
     * Todo:: The initiating
     * @param $params
     * @return bool
     */
    public function request($params) {
        // sign the parameters
        $helper = new SignatureHelper();

        // An exception may throw here, pay attension to catch
        $content = $helper->request(
            $this->keyId,
            $this->keySecret,
            "dysmsapi.aliyuncs.com",
            array_merge($params, array(
                "RegionId" => "cn-hangzhou",
                "Action" => "SendSms",
                "Version" => "2017-05-25",
            ))
        );
        $content = json_decode(json_encode($content),TRUE);
        return $content['Code'] == 'OK';
    }


}
