<?php


namespace App\Libs\Cipher\RSA;


class RsaClass
{

    private $pubKey = null;
    private $priKey = null;
    private $fileDirectory = null;

    public function __construct()
    {
        if(!extension_loaded("openssl")){
            $this->_error("Please open the openssl extension first.");
        }
    }


    /**
     * Todo:: 响应格式
     * @param string $code
     * @param bool $status
     * @param string $message
     * @param array $data
     * @return array
     */
    protected function responseArray($code='000000',$status=true,$message='Success',$data=[]){
        return array(
            'code' => $code,
            'status' => $status,
            'message' => $message,
            'data' => $data,
        );
    }

    /**
     * Todo:: 自定义返回错误
     * @param $msg
     */
    public function _error($msg){
        $message = "RSA Error: ".$msg;
        die($this->responseArray('100000',false,$message));
    }

    /**
     * Todo:: 编码 加密
     * @param $data
     * @param $code 签名编码（base64/hex/bin）
     * @return string
     */
    private function _encode($data, $code)
    {
        switch (strtolower($code)) {
            case 'base64':
                $data = base64_encode('' . $data);
                break;
            case 'hex':
                $data = bin2hex($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    /**
     * Todo:: 编码 解密
     * @param $data
     * @param $code
     * @return false|string
     */
    private function _decode($data, $code)
    {
        switch (strtolower($code)) {
            case 'base64':
                $data = base64_decode($data);
                break;
            case 'hex':
                $data = $this->_hex2bin($data);
                break;
            case 'bin':
            default:
        }
        return $data;
    }

    /**
     * Todo:: 编码 hex 解密
     * @param bool $hex
     * @return bool|false|string
     */
    private function _hex2bin($hex = false)
    {
        $ret = $hex !== false && preg_match('/^[0-9a-fA-F]+$/i', $hex) ? pack("H*", $hex) : false;
        return $ret;
    }

    /**
     * Todo:: 检测填充类型
     * 加密只支持PKCS1_PADDING
     * 解密支持PKCS1_PADDING和NO_PADDING
     *
     * @param int 填充模式
     * @param string 加密en/解密de
     * @return bool
     */
    private function _checkPadding($padding, $type)
    {
        if ($type == 'en') {
            switch ($padding) {
                case OPENSSL_PKCS1_PADDING:
                    $ret = true;
                    break;
                default:
                    $ret = false;
            }
        } else {
            switch ($padding) {
                case OPENSSL_PKCS1_PADDING:
                case OPENSSL_NO_PADDING:
                    $ret = true;
                    break;
                default:
                    $ret = false;
            }
        }
        return $ret;
    }


    // 读取文件式 公钥、私钥

    /**
     * 读取公钥和私钥
     * @param string $public_key_file 公钥文件（验签和加密时传入）
     * @param string $private_key_file 私钥文件（签名和解密时传入）
     */
    public function init($public_key_file = '', $private_key_file = '')
    {
        if ($public_key_file) {
            $this->_getPublicKey($public_key_file);
        }

        if ($private_key_file) {
            $this->_getPrivateKey($private_key_file);
        }
    }

    /**
     * Todo:: 获取公钥
     * @param $file 文件地址
     */
    private function _getPublicKey($file)
    {
        $key_content = $this->_readFile($file);
        if ($key_content) {
            $this->pubKey = openssl_get_publickey($key_content);
        }
    }

    /**
     * Todo:: 获取私钥
     * @param $file 文件地址
     */
    private function _getPrivateKey($file)
    {
        $key_content = $this->_readFile($file);
        if ($key_content) {
            $this->priKey = openssl_get_privatekey($key_content);
        }
    }

    /**
     * Todo:: 判断文件，获取文件中内容
     * @param $file
     * @return bool|false|string
     */
    private function _readFile($file)
    {
        $ret = false;
        if (!file_exists($file)) {
            $this->_error("The file {$file} is not exists");
        } else {
            $ret = file_get_contents($file);
        }
        return $ret;
    }


    // 公钥的生成、加密、解密、签名

    /**
     * 生成Rsa公钥和私钥
     * @param int $private_key_bits 建议：[512, 1024, 2048, 4096]
     * @return array
     */
    public function generate(int $private_key_bits = 1024)
    {
        $rsa = [
            "private_key" => "",
            "public_key" => ""
        ];
        $config = [
            "digest_alg" => "sha512",
            "private_key_bits" => $private_key_bits, #此处必须为int类型
            "private_key_type" => OPENSSL_KEYTYPE_RSA,
        ];
        //创建公钥和私钥
        $res = openssl_pkey_new($config);
        //提取私钥
        openssl_pkey_export($res, $rsa['private_key']);
        //生成公钥
        $rsa['public_key'] = openssl_pkey_get_details($res)["key"];
        return $rsa;
    }

    /**
     * Todo:: 生成签名
     * @param string 签名材料
     * @param string 签名编码（base64/hex/bin）
     * @return bool|string 签名值
     */
    public function sign($data, $code = 'base64')
    {
        $ret = false;
        if (openssl_sign($data, $ret, $this->priKey)) {
            $ret = $this->_encode($ret, $code);
        }
        return $ret;
    }

    /**
     * Todo:: 验证签名
     * @param string 签名材料
     * @param string 签名值
     * @param string 签名编码（base64/hex/bin）
     * @return bool
     */
    public function verify($data, $sign, $code = 'base64')
    {
        $ret = false;
        $sign = $this->_decode($sign, $code);
        if ($sign !== false) {
            switch (openssl_verify($data, $sign, $this->pubKey)) {
                case 1:
                    $ret = true;
                    break;
                case 0:
                case -1:
                default:
                    $ret = false;
            }
        }
        return $ret;
    }

    /**
     * Todo:: 加密
     * @param string 明文
     * @param string 密文编码（base64/hex/bin）
     * @param int 填充方式（貌似php有bug，所以目前仅支持OPENSSL_PKCS1_PADDING）
     * @return string 密文
     */
    public function encrypt($data, $code = 'base64', $padding = OPENSSL_PKCS1_PADDING)
    {
        $ret = false;
        if (!$this->_checkPadding($padding, 'en')) $this->_error('padding error');
        if (openssl_public_encrypt($data, $result, $this->pubKey, $padding)) {
            $ret = $this->_encode($result, $code);
        }
        return $ret;
    }

    /**
     * Todo:: 解密
     * @param string 密文
     * @param string 密文编码（base64/hex/bin）
     * @param int 填充方式（OPENSSL_PKCS1_PADDING / OPENSSL_NO_PADDING）
     * @param bool 是否翻转明文（When passing Microsoft CryptoAPI-generated RSA cyphertext, revert the bytes in the block）
     * @return string 明文
     */
    public function decrypt($data, $code = 'base64', $padding = OPENSSL_PKCS1_PADDING, $rev = false)
    {
        $ret = false;
        $data = $this->_decode($data, $code);
        if (!$this->_checkPadding($padding, 'de')) $this->_error('padding error');
        if ($data !== false) {
            if (openssl_private_decrypt($data, $result, $this->priKey, $padding)) {
                $ret = $rev ? rtrim(strrev($result), "\0") : '' . $result;
            }
        }
        return $ret;
    }

    /**
     * Todo:: 获取文件路径
     * @param $name
     * @param string $fileName
     * @return array
     */
    public function fileDirectory($name,$fileName='public_key.txt'){
        $dirString = 'rsa/'.$name.'/'.$fileName;
        $absolute = storage_path($dirString);
        return array(
            'relative' => $dirString,
            'absolute' => $absolute,
        );
    }

}
