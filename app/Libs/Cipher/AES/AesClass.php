<?php


namespace App\Libs\Cipher\AES;


class AesClass
{
    protected $key = null;
    protected $iv = null;
    protected $method = "AES-128-CBC";
    public function __construct()
    {

    }

    public static function init($type='encrypt',$data,$key,$iv,$method='AES-128-CBC',$options=0){
        $type = strtolower($type);
        if($type == 'decrypt'){
            //        加密数据 'AES-128-CBC' 可以通过openssl_get_cipher_methods()获取
            $res = openssl_encrypt($data, $method, $key, 0, $iv);
        }else{
            // 解密数据
            $res = openssl_decrypt($data, 'AES-128-CBC', $key, 0, $iv);
        }
        return $res;
    }


    // 本项目中使用

}
