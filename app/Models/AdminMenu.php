<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminMenu extends Model
{
    public $dateFormat = 'U';
    public $timestamps = false;
    protected $table = 'admin_menu';
    public $is_display_arr = ['0' => '不显示', '1' => '显示'];
    public $write_log_arr = ['0' =>'不记录', '1' => '记录'];
    protected $guarded = []; //不可以注入
    public $fillable = ['menu_name', 'parent_id', 'icon','m', 'v','address', 'data','is_type', 'is_display', 'list_order','write_log','creator_user']; //可以注入
    public $messages = [
        'menu_name.required' => '名称不能为空',
        'menu_name.max' => '名称范围值2~30',
        'menu_name.min' => '名称范围值2~30',
        'm.required' => '版本不能为空',
        'v.required' => '版本不能为空',
        'address.required' => '访问地址不能为空',
    ];
    public $rules = [
        'menu_name' => 'required|string|max:30|min:2',
        'm' => 'required|string',
        'v' => 'required|string',
        'address' => 'required|string',
    ];


    /**
     * Todo:: 菜单状态
     * @param int $code
     * @return array|mixed|string
     */
    public function isDisplayArray($code=-1){
        $data = array(
            0 => '隐藏',
            1 => '显示'
        );
        if($code != -1){
            if(isset($data[$code])){
                return $data[$code];
            }else{
                return "";
            }
        }
        return $data;
    }

    /**
     * Todo:: 是否记录日志
     * @param int $code
     * @return array|mixed|string
     */
    public function writeLogArray($code=-1){
        $data = array(
            0 => '不记录',
            1 => '记录'
        );
        if($code != -1){
            if(isset($data[$code])){
                return $data[$code];
            }else{
                return "";
            }
        }
        return $data;
    }

    /**
     * Todo:: 菜单类型
     * @param int $code
     * @return array|mixed|string
     */
    public function isTypeArray($code=-1){
        $data = array(
//            0 => '未知',
            1 => '导航',
            2 => '操作',
        );
        if($code != -1){
            if(isset($data[$code])){
                return $data[$code];
            }else{
                return "";
            }
        }
        return $data;
    }

    /**
     * 全部菜单
     * @param array $where
     * @return mixed
     */
    public function getAllMenu($where = []){
        return static::where($where)->orderBy('list_order', 'asc')->get()->toArray();
    }

    /**
     * Todo:: 查询菜单所有子级
     * @param $id
     * @return array
     */
    public function getMenuChild($id){
        $pId = intval($id);
        $res = DB::select("select getMenuChild({$pId},{$this->table}) as ids");
        dd($res);
        $data = [];
        if($res){
            $array = explode(',',$res[0]->ids);
            foreach ($array as $key=>$value){
                if(!empty($value)){
                    $data[]=$value;
                }
            }
            unset($value);
        }
        return $data;
    }

    /**
     * 我的菜单
     * @param int $status 状态 1 只查显示,0所有
     * @return array|bool
     */
    public function adminMenu($status = 1)
    {
        $where = array();
        if ($status == 1) {
            $where[] = ['is_display', '=', 1];
        }
        $adminUser = request()->user('admin');
        //查看此人是否超级管理员组,如果是返回所有权限
        if ($adminUser->is_super == 1 || $adminUser->id == 1) {
            //超级管理员
            $menus = static::where($where)->orderBy('list_order', 'asc')->get()->toArray();
        } else {
            //查出用户所在组Id拥有的menus
            $menus = $this->adminGroupMenu($adminUser->id,$status);
        }
        return $menus;
    }


    /**
     * Todo:: 获取管理员 菜单
     * @param $adminId
     * @param int $menuStatus
     * @return array|\Illuminate\Database\Query\Builder
     */
    public function adminGroupMenu($adminId,$menuStatus=1){
        $data = DB::table('admin_group_access')
            ->leftJoin('admin_group','admin_group_access.group_id','=','admin_group.id')
            ->leftJoin('admin_group_menu','admin_group_menu.group_id','=','admin_group.id')
            ->leftJoin('admin_menu','admin_group_menu.menu_id','=','admin_menu.id')
            ->select(array('admin_menu.*'))
            ->where([
                ['admin_group_access.admin_id','=',$adminId],
                ['admin_group.status','=',1],
            ]);
        if($menuStatus){
            $data->where('admin_menu.is_display','=',1);
        }
        $data = $data->orderBy('admin_menu.list_order', 'asc')->distinct()->get()->toArray();
        return $data;
    }


    /**
     * Todo:: 所有操作菜单
     */
    public static function getMenuList($where=[])
    {
        $res = static::where($where)->orderBy('list_order', 'asc')->get()->toArray();
        $res = nodeTree($res);
        return $res;
    }

    /**
     * Todo:: 下拉框菜单选择
     * @param array $where
     * @return array
     */
    public function selectMenu($where=[])
    {
        $tmpArr = $this->getMenuList($where);
        $data = array();
        foreach ($tmpArr as $k => $v) {
            $name = $v['level'] == 0 ? '<b>' . $v['menu_name'] . '</b>' : '├─' . $v['menu_name'];
            $name = str_repeat("│        ", $v['level']) . $name;
            $data[$v['id']] = $name;
        }
        return $data;
    }

}
