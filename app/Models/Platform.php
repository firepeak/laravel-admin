<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Platform extends Model
{
    protected $table = 'platform';
    public $timestamps = false; // 指示模型是否自动维护时间戳
    protected $dateFormat = 'U'; // 模型日期列的存储格式。
    public $fillable = ['account','password','pwd_salt','name','ip','domain_name','sign_key','aes_key','aes_vector','status','introduction']; //可以注入

    /**
     * Todo:: 状态数组
     * @param int $code
     * @return array|mixed
     */
    public function statusArray($code=-1){
        $data = [
            0 => '禁用',
            1 => '正常',
        ];
        if($code != -1 && isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 判断名称是否存在
     * @param $account
     * @return mixed
     */
    public function isAccount($account){
        return self::where('account',$account)->value('id');
    }


    /**
     * Todo:: 日志记录判断条件
     * @param array $conditions
     * @param array $select
     * @return \Illuminate\Database\Query\Builder
     */
    public function dataWhere($conditions=[],$select=[]){
        $data = DB::table('platform');

        if($select){
            $data->select($select);
        }else{
            $data->select(array('platform.*'));
        }
        if(isset($conditions['keyword']) && !empty($conditions['keyword'])){
            $keyword = $conditions['keyword'];
            $data->orWhere(function ($query) use ($keyword) {
                $query->orWhere('platform.account', 'like', "%{$keyword}%")
                    ->orWhere('platform.name', 'like', "%{$keyword}%")
                    ->orWhere('platform.ip', 'like', "%{$keyword}%")
                    ->orWhere('platform.domain_name', 'like', "%{$keyword}%");
            });
        }
        if(isset($conditions['id']) && is_numeric($conditions['id'])){
            $data->where('platform.id',$conditions['id']);
        }
        if(isset($conditions['status']) && is_numeric($conditions['status'])){
            $data->where('platform.status',$conditions['status']);
        }
        if(isset($conditions['account']) && $conditions['account']){
            $data->where('platform.account',$conditions['account']);
        }
        if(isset($conditions['name']) && $conditions['name']){
            $data->where('platform.name',$conditions['name']);
        }
        if (isset($conditions['ip']) && $conditions['ip']) {
            $data->where('platform.ip', $conditions['ip']);
        }
        if (isset($conditions['start_time']) && !empty($conditions['start_time']) && isset($conditions['end_time']) && !empty($conditions['end_time'])) {
            $data->whereBetween('platform.created_at', [$conditions['start_time'],$conditions['end_time']]);
        }else{
            if(isset($conditions['start_time']) && !empty($conditions['start_time'])){
                $data->where('platform.created_at','>',$conditions['start_time']);
            }elseif(isset($conditions['end_time']) && !empty($conditions['end_time'])){
                $data->where('platform.created_at','<',$conditions['end_time']);
            }
        }
        return $data;
    }

    /**
     * Todo:: 获取日志列表数据
     * @param array $conditions
     * @param array $select
     * @param array $orderArr
     * @param int $page
     * @param int $pageNum
     * @param string $excelType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginateLists($conditions=[],$select=[],$orderArr=[],$page=1,$pageNum=20,$excelType=''){
        $dataWhere = $this->dataWhere($conditions,$select);
        if ($orderArr) {
            foreach ($orderArr as $order) {
                $dataWhere->orderBy($order['field'], $order['direction']);
            }
        } else {
            $dataWhere->orderBy('platform.id', 'desc');
        }
        if($excelType == 'all'){
            // 导出
            $data = $dataWhere->get();
        }else{
            $data = $dataWhere->paginate($pageNum,['*'],'page',$page);
        }
        return $data;
    }


}
