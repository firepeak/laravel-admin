<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AdminLog extends Model
{
    public $dateFormat = 'U';
    public $timestamps = false;
    protected $table = 'admin_log';
    protected $guarded = []; //不可以注入
    public $fillable = ['menu_id','method','primary_id','query_string','type','data','ip','admin_id']; //可以注入

    /**
     * Todo:: 日志类型
     * @param int $code
     * @return array|mixed
     */
    public function typeArr($code=0){
        $data = array(
            1 => '记录日志',
            2 => '登录',
            3 => '登出'
        );
        if(isset($data[$code])){
            return $data[$code];
        }
        return $data;
    }

    /**
     * Todo:: 添加日志
     * @param $admin_id
     * @param $data
     * @param int $type
     * @param int $menu_id
     * @param int $primary_id
     * @param string $query_string
     * @return mixed
     */
    public function addLogs($admin_id,$data,$type=1,$menu_id=0,$primary_id=0,$query_string=''){
        $info = [
            'menu_id'=>$menu_id,
            'primary_id'=>$primary_id,
            'query_string'=>$query_string,
            'type'=>$type,
            'data'=>$data,
            'ip'=>request()->ip(),
            'admin_id'=>$admin_id,
        ];
        $res = $this->create($info);
        return $res;
    }

    /**
     * Todo:: 日志记录判断条件
     * @param array $conditions
     * @param array $select
     * @return \Illuminate\Database\Query\Builder
     */
    public function dataWhere($conditions=[],$select=[]){
        $data = DB::table('admin_log')
            ->leftJoin('admin_menu','admin_log.menu_id','=','admin_menu.id')
            ->leftJoin('admin_user','admin_log.admin_id','=','admin_user.id');

        if($select){
            $data->select($select);
        }else{
            $data->select('admin_log.*',
                'admin_menu.menu_name','admin_menu.m','admin_menu.v','admin_menu.address',
                'admin_user.username as admin_name','admin_user.realname');
        }
        if(isset($conditions['keyword']) && !empty($conditions['keyword'])){
            $keyword = $conditions['keyword'];
            $data->orWhere(function ($query) use ($keyword) {
                $query->orWhere('admin_menu.menu_name', 'like', "%{$keyword}%")
                    ->orWhere('admin_user.username', 'like', "%{$keyword}%");
            });
        }
        if(isset($conditions['id']) && is_numeric($conditions['id'])){
            $data->where('admin_log.id',$conditions['id']);
        }
        if(isset($conditions['menu_name']) && $conditions['menu_name']){
            $data->where('admin_menu.menu_name',$conditions['menu_name']);
        }
        if(isset($conditions['type']) && $conditions['type']){
            $data->where('admin_log.type',$conditions['type']);
        }
        if(isset($conditions['username']) && $conditions['username']){
            $data->where('admin_menu.username',$conditions['username']);
        }
        if (isset($conditions['ip']) && $conditions['ip']) {
            $data->where('admin_log.ip', $conditions['ip']);
        }
        if (isset($conditions['start_time']) && !empty($conditions['start_time']) && isset($conditions['end_time']) && !empty($conditions['end_time'])) {
            $data->whereBetween('admin_log.created_at', [$conditions['start_time'],$conditions['end_time']]);
        }else{
            if(isset($conditions['start_time']) && !empty($conditions['start_time'])){
                $data->where('admin_log.created_at','>',$conditions['start_time']);
            }elseif(isset($conditions['end_time']) && !empty($conditions['end_time'])){
                $data->where('admin_log.created_at','<',$conditions['end_time']);
            }
        }
        return $data;
    }

    /**
     * Todo:: 获取日志列表数据
     * @param array $conditions
     * @param array $select
     * @param array $orderArr
     * @param int $page
     * @param int $pageNum
     * @param string $excelType
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function paginateLists($conditions=[],$select=[],$orderArr=[],$page=1,$pageNum=20,$excelType=''){
        $dataWhere = $this->dataWhere($conditions,$select);
        if ($orderArr) {
            foreach ($orderArr as $order) {
                $dataWhere->orderBy($order['field'], $order['direction']);
            }
        } else {
            $dataWhere->orderBy('admin_log.id', 'desc');
        }
        if($excelType == 'all'){
            // 导出
            $data = $dataWhere->get();
        }else{
            $data = $dataWhere->paginate($pageNum,['*'],'page',$page);
        }
        return $data;
    }

    /**
     * Todo:: 获取日志详情
     * @param $logId
     * @return Model|\Illuminate\Database\Query\Builder|object|null
     */
    public function getInfo($logId){
        $dataWhere = $this->dataWhere(array('id'=>$logId));
        return $dataWhere->first();
    }

}
