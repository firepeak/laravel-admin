<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class UsersInvite extends Model
{
    protected $table = 'users_invite';
    public $timestamps = false; // 指示模型是否自动维护时间戳
    protected $dateFormat = 'U'; // 模型日期列的存储格式。
    public $fillable = ['user_id','invite_code','recommend_id']; //可以注入

    /**
     * Todo:: 软删除
     * @param $userId
     * @return mixed
     */
    public function updateDel($userId){
        return self::where('user_id',$userId)->update([
            'is_del' => 1,
            'deleted_at' => date('Y-m-d H:i:s')
        ]);
    }

    /**
     * Todo:: 邀请的总人数
     * @param $userId
     * @param int $is_del
     * @return mixed
     */
    public function inviteTotal($userId,$is_del=0){
        return self::where('recommend_id',$userId)->where('is_del',$is_del)->count();
    }
}
