<!doctype html>
<html>
<head>
<meta charset="UTF-8" />
<title><?php echo $Title; ?> - <?php echo $Powered; ?></title>
<link rel="stylesheet" href="./css/install.css?v=9.0" />
<script src="js/jquery.js"></script>
<?php
$uri = $_SERVER['REQUEST_URI'];
$root = substr($uri, 0,strpos($uri, "install"));
$admin = $root."../admin";
?>
</head>
<body>
<div class="wrap">
  <?php require './templates/header.php';?>
  <section class="section">
    <div class="">
        <div class="success_tip cc"><a href="<?php echo $adminLoginUrl; ?>" class="f16 b">安装完成</a>
            <p><strong style="color: red">注意：</strong></p>
            <p>1、为了您站点的安全，安装完成后即可将网站根目录下的“install”文件夹删除，或者/install/目录下创建install.lock文件防止重复安装。</p>
            <p>2、请在30天内登录后台。当前后台密码为md5加密，登录后自动更新为哈希加密</p>
            <p>3、后台登录地址：<?php echo $adminLoginUrl; ?></p>
            <p>4、后台登录地址添加 token ;详情查看说明文档 README.md</p>
        </div>
	        <div class="bottom tac">
	        <a href="/" class="btn">进入前台</a>
	        <a href="<?php echo $adminLoginUrl; ?>" class="btn btn_submit J_install_btn">进入后台</a>
      </div>
      <div class=""> </div>
    </div>
  </section>
</div>
<?php require './templates/footer.php';?>
<!--
<script>
$(function(){
	$.ajax({
	type: "POST",
	url: "http://service.tp-shop.cn/index.php?m=Home&c=Index&a=user_push",
	data: {domain:'<?php echo $host;?>',last_domain:'<?php echo $host?>',key_num:'<?php echo $curent_version;?>',install_time:'<?php echo $time;?>',serial_number:'<?php echo $mt_rand_str;?>'},
	dataType: 'json',
	success: function(){}
	});
});
</script>
-->
</body>
</html>
