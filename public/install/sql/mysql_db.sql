-- MySQL dump 10.13  Distrib 5.6.40, for macos10.13 (x86_64)
--
-- Host: 127.0.0.1    Database: laravel_admin_v002
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `laravel_admin_v002`
--

-- CREATE DATABASE /*!32312 IF NOT EXISTS*/ `laravel_admin_v002` /*!40100 DEFAULT CHARACTER SET utf8 */;
--
-- USE `laravel_admin_v002`;

--
-- Table structure for table `admin_group`
--

DROP TABLE IF EXISTS `admin_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_group` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(200) DEFAULT NULL COMMENT '角色描述',
  `list_order` smallint(3) NOT NULL DEFAULT '900' COMMENT '排序',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态;0-禁用,1-启用',
  `updated_user` int(11) NOT NULL DEFAULT '0' COMMENT '最近更新操作用户',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员角色组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_group`
--

LOCK TABLES `admin_group` WRITE;
/*!40000 ALTER TABLE `admin_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_group_access`
--

DROP TABLE IF EXISTS `admin_group_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_group_access` (
  `admin_id` int(11) NOT NULL DEFAULT '0' COMMENT '管理员ID',
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  UNIQUE KEY `admin_group` (`admin_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员权限组';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_group_access`
--

LOCK TABLES `admin_group_access` WRITE;
/*!40000 ALTER TABLE `admin_group_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_group_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_group_menu`
--

DROP TABLE IF EXISTS `admin_group_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_group_menu` (
  `group_id` int(11) NOT NULL DEFAULT '0' COMMENT '角色组ID',
  `menu_id` int(11) NOT NULL DEFAULT '0' COMMENT '菜单表ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  UNIQUE KEY `group_menu` (`group_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='管理员角色组与菜单关联';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_group_menu`
--

LOCK TABLES `admin_group_menu` WRITE;
/*!40000 ALTER TABLE `admin_group_menu` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_group_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_log`
--

DROP TABLE IF EXISTS `admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` mediumint(8) NOT NULL DEFAULT '0' COMMENT '菜单id',
  `primary_id` int(11) DEFAULT '0' COMMENT '表中主键ID',
  `method` varchar(10) NOT NULL DEFAULT '' COMMENT '请求方式',
  `query_string` varchar(255) DEFAULT '' COMMENT '请求参数',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '属性1-记录日志,2-登录,3-退出登录',
  `data` text COMMENT 'POST数据',
  `ip` varchar(18) NOT NULL DEFAULT '',
  `admin_id` mediumint(8) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_menu_id` (`menu_id`),
  KEY `idx_admin_id` (`admin_id`),
  KEY `idx_created_at` (`created_at`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='管理员操作日志';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_log`
--

LOCK TABLES `admin_log` WRITE;
/*!40000 ALTER TABLE `admin_log` DISABLE KEYS */;
INSERT INTO `admin_log` VALUES (1,0,0,'','',2,'登录','127.0.0.1',1,'2020-08-21 10:22:14',NULL);
/*!40000 ALTER TABLE `admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_menu`
--

DROP TABLE IF EXISTS `admin_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_menu` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `menu_name` varchar(30) NOT NULL DEFAULT '' COMMENT '菜单名称',
  `icon` varchar(20) DEFAULT NULL COMMENT '图标',
  `m` varchar(20) NOT NULL DEFAULT 'admin' COMMENT '模板名',
  `v` varchar(20) NOT NULL DEFAULT 'v1' COMMENT '版本',
  `address` varchar(60) NOT NULL DEFAULT '' COMMENT '访问地址',
  `data` varchar(200) DEFAULT NULL COMMENT '额外参数',
  `list_order` smallint(3) NOT NULL DEFAULT '999' COMMENT '排序',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT '父级ID,引用本表id',
  `updated_user` int(11) NOT NULL DEFAULT '0' COMMENT '最近更新操作用户',
  `is_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示0-未知,1-导航,2-操作',
  `is_display` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示0-隐藏,1-显示',
  `write_log` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否记录日志0-不记录,1-记录',
  `creator_user` int(11) NOT NULL DEFAULT '0' COMMENT '创造者ID',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `admin_group` (`m`,`v`,`address`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='后台菜单';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_menu`
--

LOCK TABLES `admin_menu` WRITE;
/*!40000 ALTER TABLE `admin_menu` DISABLE KEYS */;
INSERT INTO `admin_menu` VALUES (1,'主页','fa-home','admin','v1','home/index',NULL,1,0,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(2,'系统管理','fa-cogs','admin','v1','system/index',NULL,900,0,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(3,'后台菜单管理','fa-tachometer','admin','v1','system/menu/lists',NULL,1,2,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(4,'创建菜单','','admin','v1','system/menu/create',NULL,1,3,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(5,'编辑菜单','','admin','v1','system/menu/edit',NULL,2,3,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(6,'删除菜单','','admin','v1','system/menu/del',NULL,3,3,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(7,'菜单详情','','admin','v1','system/menu/info',NULL,4,3,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(8,'系统配置','fa-cog','admin','v1','system/config/index',NULL,2,2,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(9,'配置保存','','admin','v1','system/config/save',NULL,1,8,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(10,'清理缓存','','admin','v1','system/config/clean_cache',NULL,10,8,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(11,'日志管理','fa-calendar','admin','v1','log/index',NULL,800,0,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(12,'管理员日志','fa-calendar','admin','v1','admin_user/log/lists',NULL,800,11,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(13,'管理员日志详情','','admin','v1','admin_user/log/info',NULL,1,12,0,2,1,0,0,'2020-08-21 10:19:54',NULL),(14,'管理员','fa-key','admin','v1','admin_user/index',NULL,888,0,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(15,'管理员角色','fa-fire','admin','v1','admin_user/group/lists',NULL,2,14,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(16,'创建管理员角色','','admin','v1','admin_user/group/create',NULL,1,15,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(17,'编辑管理员角色','','admin','v1','admin_user/group/edit',NULL,2,15,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(18,'删除管理员角色','','admin','v1','admin_user/group/del',NULL,3,15,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(19,'管理员详情角色','','admin','v1','admin_user/group/info',NULL,4,15,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(20,'管理员列表','fa-users','admin','v1','admin_user/index/lists',NULL,1,14,0,1,1,0,0,'2020-08-21 10:19:54',NULL),(21,'创建管理员','','admin','v1','admin_user/index/create',NULL,1,20,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(22,'编辑管理员','','admin','v1','admin_user/index/edit',NULL,2,20,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(23,'删除管理员','','admin','v1','admin_user/index/del',NULL,3,20,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(24,'管理员详情','','admin','v1','admin_user/index/info',NULL,4,20,0,2,1,1,0,'2020-08-21 10:19:54',NULL),(25,'编辑密码','','admin','v1','admin_user/index/pwd',NULL,5,20,0,2,1,1,0,'2020-08-21 10:19:54',NULL);
/*!40000 ALTER TABLE `admin_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(200) NOT NULL COMMENT '密码',
  `salt` varchar(16) NOT NULL DEFAULT '' COMMENT '加盐',
  `email` varchar(40) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '电话',
  `realname` varchar(20) DEFAULT NULL COMMENT '真实姓名',
  `head_portrait` varchar(100) DEFAULT NULL COMMENT '头像地址',
  `introduction` varchar(200) DEFAULT NULL COMMENT '简介',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态0-禁用,1正常',
  `level` tinyint(2) NOT NULL DEFAULT '0' COMMENT '等级',
  `is_super` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否为超级管理员1是',
  `creator` int(10) NOT NULL DEFAULT '0' COMMENT '创始人,0初始化',
  `reg_ip` varchar(15) NOT NULL DEFAULT '0.0.0.0' COMMENT '创建IP',
  `remember_token` varchar(100) DEFAULT NULL COMMENT '存储当用户登录应用并勾选「记住我」时的令牌',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted_at` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='管理员';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `config`
--

DROP TABLE IF EXISTS `config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `config` (
  `name` varchar(30) NOT NULL DEFAULT 'not' COMMENT '配置变量名称',
  `alias` varchar(30) NOT NULL DEFAULT 'not' COMMENT '配置变量别名',
  `value` varchar(225) NOT NULL DEFAULT 'not' COMMENT '配置变量值',
  `type_name` varchar(30) NOT NULL DEFAULT 'not' COMMENT '类型名称',
  `explanation` varchar(30) NOT NULL DEFAULT 'not' COMMENT '配置变量说明',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted_at` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='配置表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `config`
--

LOCK TABLES `config` WRITE;
/*!40000 ALTER TABLE `config` DISABLE KEYS */;
/*!40000 ALTER TABLE `config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-21 11:39:45
